#!/bin/bash
set -u

MYCNF_PATH="/etc/mysql/my.cnf"
DEFAULT_MYSQL_ERROR_DIR="/var/log/mysql"


if [ ! -v MYMODE ] || [ ${#MYMODE} -lt 1 ]; then
    echo "lack of parameter MYMODE"
    exit 1
fi

# diverge if RESTORE or REPOSITORY
if [ ${MYMODE} = 'restore' ]; then
  # restore mode
  echo "restore mode"
  chmod 644 ${MYCNF_PATH}


  if [ -v MYDUMPER ] && [ ${MYDUMPER} -eq 1 ]; then

    # TODO: thread num
    if [ -v MYSQL_DATABASE ] && [ ${#MYSQL_DATABASE} -gt 1 ]; then
        MYSQL_DATABASE="--database "${MYSQL_DATABASE}
    else
        MYSQL_DATABASE=""
    fi

    myloader -u ${MYSQL_USER} -p ${MYSQL_ROOT_PASSWORD} -h ${MYSQL_HOST} ${MYSQL_DATABASE} -o -d ${RESTORE_SQL}
  else
    mysql -u${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -h${MYSQL_HOST} ${MYSQL_DATABASE} < ${RESTORE_SQL}
  fi

elif [ ${MYMODE} = 'dump' ]; then
  # dump mode
  echo "dump mode"

  # whether database is specified or not
  if [ -v MYSQL_DATABASE ] && [ ${#MYSQL_DATABASE} -gt 1 ]; then
    if [ -v MYDUMPER ] && [ ${MYDUMPER} -eq 1 ]; then
        MYSQL_DATABASE="--database "${MYSQL_DATABASE}
    fi
    echo "database is specified."
  else
    echo "no database"
    MYSQL_DATABASE=""
  fi

  # whether tables is specified or not
  if [ -v DUMP_TABLES ] && [ ${#DUMP_TABLES} -gt 1 ]; then
    echo "tables is specified"
    if [ -v MYDUMPER ] && [ ${MYDUMPER} -eq 1 ]; then
        DUMP_TABLES="--tables-list "`cat ${DUMP_TABLES}| sed -e "s/\s\s*/,/g"`
    else
        DUMP_TABLES="--tables "${DUMP_TABLES}
    fi
  else
    echo "no table"
    DUMP_TABLES=""
  fi

  # other mysqldump options
  # TODO: it doesn't apply to mydumper 2016-05-21
  if [ -v DUMP_OPTIONS ] && [ ${#DUMP_OPTIONS} -gt 1 ]; then
    echo "dump options is specified"
  else
    echo "no dump option"
    DUMP_OPTIONS=""
  fi

  # thread num.  only in case of mydumper
  # ------

  chmod 644 ${MYCNF_PATH}

  echo "user:${MYSQL_USER} pass:${MYSQL_ROOT_PASSWORD} host:${MYSQL_HOST} db:${MYSQL_DATABASE} tables:${DUMP_TABLES} options:${DUMP_OPTIONS} dump-to:${DUMP_TO}"
  if [ -v MYDUMPER ] && [ ${MYDUMPER} -eq 1 ]; then
    mydumper -u ${MYSQL_USER} -p ${MYSQL_ROOT_PASSWORD} -h ${MYSQL_HOST} ${MYSQL_DATABASE} ${DUMP_TABLES} -o ${DUMP_TO}
  else
    mysqldump -u${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -h${MYSQL_HOST} ${MYSQL_DATABASE} ${DUMP_TABLES} ${DUMP_OPTIONS} > ${DUMP_TO}
  fi


elif [ ${MYMODE} = 'connect' ]; then
  # connect mode
  echo "trying to connect remote mysql"
  chmod 644 ${MYCNF_PATH}
  mysql -u${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -h${MYSQL_HOST}

elif [ ${MYMODE} = 'server' ]; then
  echo "server mode"
  ###################### setup mysql server ############################

  # general log
  if [ -v GENERAL_LOG ] && [ ${GENERAL_LOG} -eq 1 ]; then
    echo "general log on"
    sed -i -e "s/___GENERAL_LOG_ON___/general_log = 1/" ${MYCNF_PATH}
    sed -i -e "s|___GENERAL_LOG_TO___|general_log_file = /var/log/mysql/general.log|" ${MYCNF_PATH}

    # log_output  FILE or TABLE or FILE,TABLE
    if [ -v LOG_OUTPUT ] && [ ${#LOG_OUTPUT} -gt 1 ]; then
        echo "log_output specified"
        sed -i -e "s/___LOG_OUTPUT___/log_output = ${LOG_OUTPUT}/" ${MYCNF_PATH}
    else
        echo "no log_output"
        sed -i -e "/___LOG_OUTPUT___/d" ${MYCNF_PATH}
    fi

  else

    sed -i -e "s/___GENERAL_LOG_ON___/general_log = 0/" ${MYCNF_PATH}
    sed -i -e "/___GENERAL_LOG_TO___/d" ${MYCNF_PATH}
    sed -i -e "/___LOG_OUTPUT/d" ${MYCNF_PATH}

  fi

  # set permission of error_log
  chmod -R 777 ${DEFAULT_MYSQL_ERROR_DIR}

  if [ -v TARGET_REPOSITORY ] && [ ${#TARGET_REPOSITORY} -gt 1 ]; then
      git clone --depth=1 -b $TARGET_BRANCH $TARGET_REPOSITORY /tmp
      mv /tmp/$TARGET_SQL /docker-entrypoint-initdb.d/$TARGET_SQL
  fi

  chmod 644 ${MYCNF_PATH}
  /entrypoint.sh mysqld
fi
