FROM mysql:5.7
MAINTAINER corginia keisuke satomi <mlh38048@yahoo.co.jp>

RUN apt-get update && apt-get install -y git mydumper

COPY ./my.cnf /etc/mysql/my.cnf
COPY ./sql.sh /root/sql.sh
RUN chmod ugo+x /root/sql.sh

ENTRYPOINT ["/root/sql.sh"]
